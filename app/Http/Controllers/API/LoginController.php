<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    /**
     * Method to login with email and password
     */
    function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $params = [
            'grant_type' => 'password',
            'client_id' => 'client-id',
            'client_secret' => 'client-secret',
            'username' => $request->email,
            'password' => $request->password,
            'scope' => '',
        ];

        $request->request->add($params);
        $proxy=Request::create('oauth/token', 'POST');
        return Route::dispatch($proxy);
    }
}
